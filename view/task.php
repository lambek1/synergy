<?php
$t = '
<div class="container">
  <div class="row">
    <div class="col-sm-12">
        <a href="/"><h3>Вернуться к заданию</h3></a>
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-12 bg-light py-2">
        <h4>sql запрос к заданию:</h4>
        <i>'.$arraydata["task"].'</i>
        <p>
            '.$arraydata["str"].'
        </p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-12 py-2">
           <table class="table">
          <thead>
            <tr>    
              <th scope="col">id</th>
              <th scope="col">Имя</th>
              <th scope="col">Средний бал</th>      
            </tr>
          </thead>
          <tbody>
';

if(isset($arraydata['table'])){
    foreach ($arraydata['table'] as $e){
        $t .= '<tr>              
              <td>'.$e["ID"].'</td>
              <td>'.$e["name"].'</td>
              <td>'.$e["grade"].'</td>      
            </tr>
        ';
    }
}


$t .= '          </tbody>
        </table>
    </div>
</div>
<form class="form-inline" action="/task-1-1.php">
  <div class="form-group mb-2">
    <label for="string_list_l" class="sr-only"></label>
    <input type="text" readonly class="form-control-plaintext"  value="Показать записей"  id="string_list_l">
  </div>
  <div class="form-group mx-sm-3 mb-2">
 <select class="custom-select" name="select">    
    <option value=""'.($arraydata['selected']?"selected":"").'>Все</option>
    <option value="10"'.($arraydata['selected']==10?"selected":"").'>10</option>
    <option value="20" '.($arraydata['selected']==20?"selected":"").'>20</option>
  </select>        
  </div>
  <button type="submit" class="btn btn-primary mb-2">Показать</button>
</form>
';

return $t;