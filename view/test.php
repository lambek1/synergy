<?php
return '
<div class="container">
  <div class="row">
    <div class="col-sm">
   <p> Социальная сеть
        Допустим, мы разрабатываем социальную сеть для студентов. У нас есть 2 таблицы в базе данных.
        Students (ID, name, grade) - студенты (ID, имя, средний балл)
        Likes (like_ID, liked_ID) - лайки одного студента страницы другого. like_ID - это ID того, кто поставил лак. liked_ID - ID того, кого "лайкнули".
        Нужно выполнить несколько задач.
        <ul>
            <li>Получить имена и средний балл всех студентов, которые были "лайкнуты" более чем одним студентом. <a href="/task-1-1.php">Решение</a></li>
            <li>Получить имена и средний балл студентов А, которые лайкнули студентов В, но при этом студенты В не поставили лайк ни на одной из страниц других студентов. <a href="/task-1-2.php">Решение</a></li>
            <li>Вернуть имена и средний балл всех студентов, которые не лайкали чужие страницы и не были лайкнуты другими пользователями. <a href="/task-1-3.php">Решение</a></li>
        </ul>
        Реализуйте все три пункта с помощью PHP и SQL запросов.
   </p>
   <p>     Новости  <a href="/task-2.php">Решение</a> <br/>
        Пусть на главной странице сайта у нас есть виджет новостей, где выводятся три последних по времени добавления новости. При этом в базе данных миллионы новостей и запрос с сортировкой ORDER BY id DESC для каждого вызова страницы будет работать медленно. Напишите небольшой код этого виджета на PHP, который позволил бы обойти эту ситуацию.
     </p>    
   <p>
        Почтовые сообщения <a href="/task-3.php">Решение</a><br/>
  
   
        Представьте, что вам нужно реализовать рассылку почтовых уведомлений для социальной сети. Это уведомления о новых сообщениях, уведомления о регистрации, восстановления пароля, запросы на добаления в друзья и т.д. Объемы порядка 10 млн. писем в сутки. О каждом письме необходимо вести статистику: было ли доставлено, если не было, то почему, а так же пробовать отправить повторно, если в первый раз не вышло. Опишите примерно, как бы вы решали эту задачу? Какие технологии и программы использовали бы, а так же, как построили бы архитектуру?
   </p>
   <p>
        Выкатка    
        <ul>
            <li>У нас существует система требующая бесперебойной работы.</li>
            <li>Обновления выходят раз в неделю.</li>
            <li>Несколько серверов бэкэндов расположены за балансировщиком.</li>
            <li>Предложите схему обновления всех бэкэндов.</li>
            <li>Что делать если в релизе была допущена ошибка?</li>
            <li>Оцените время на деплой релиза и на откат изменений.</li>
            <li>Как Вы организовали бы деплой изменений в бд?</li>
        </ul>
   </p>
   <p>
        Сортировка  <a href="/task-5.php">Решение</a><br/>
        На входе есть одномерный масив вида [‘asd’,’qwe’,’sda’,’wqe’,’dsa’,’qwer’,’afds’,’zxc’]
        Требуется массив сгруппировать по группам слов состоящих из одних и тех же букв.
        Пример правильного ответа:
        [
            [‘asd’,’sda’,’dsa’],
            [‘qwe’,’wqe’],
            [‘qwer’],
            [‘afds],
            [‘zxc’]
        ]
        </p>
  </div>
</div>
';