<?php
$t = '
<div class="container">
  <div class="row">
    <div class="col-sm-12">
        <a href="/"><h3>Вернуться к заданию</h3></a>
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-12 bg-light py-2">
        <h4>sql запрос к заданию:</h4>
        <i>'.$arraydata["task"].'</i>
        <p>
            '.$arraydata["str"].'
        </p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-12 py-2"><ul>';

if(isset($arraydata['news'])){
    foreach ($arraydata['news'] as $e){
        $t .= '<li>'.$e["name_news"].'<br/><i style="font-size:12px; ">'.$e["date_publication"].'</i></li>';
    }
}

$t .= '</ul>        
    </div>
</div>

';

return $t;