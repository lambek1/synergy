<?php

include_once 'bootstrap.php';
include_once 'Rend.php';

$bootstrap = new bootstrap();
$db = $bootstrap->getDb();
$view = new Rend();

$selected = (isset($_GET ['select']) && is_numeric($_GET ['select'])) ? " LIMIT 0," . $_GET ['select'] : ' LIMIT 0,10;';

$str = 'SELECT `likes`.`liked_ID` FROM `likes` GROUP BY `likes` .`liked_ID`' . $selected;
$array_1 = $db->query($str, PDO::FETCH_ASSOC)->fetchAll();

$str = 'SELECT `likes`.`like_ID` FROM `likes` GROUP BY `likes` .`like_ID`' . $selected;
$array_2 = $db->query($str, PDO::FETCH_ASSOC)->fetchAll();

$temp_array_1 = [];
foreach ($array_1 as $e) {
    $temp_array_1[] = $e['liked_ID'];
}
foreach ($array_2 as $e) {
    $key = array_search($e['like_ID'], $temp_array_1);
    unset($temp_array_1[$key]);
}
$str_id = implode(",", $temp_array_1);
unset($temp_array_1, $array_1, $array_2);

$str = "SELECT * FROM `students` WHERE `students`.`ID` iN ($str_id)";
$table = $db->query($str, PDO::FETCH_ASSOC)->fetchAll();

echo (string)$view->view("task", [
    'selected' => isset($_GET ['select']) ? $_GET ['select'] : 10,
    'table' => $table,
    'task' => "Получить имена и средний балл студентов А, которые лайкнули студентов В, но при этом студенты В не поставили лайк ни на одной из страниц других студентов.",
    'str' => "",
]);

