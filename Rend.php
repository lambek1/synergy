<?php


class Rend
{

    public function view($nameTemplate, $arraydata)
    {
        $t_1 = file_get_contents(__DIR__.'/view/headr.php');
        $t_2 = file_get_contents(__DIR__.'/view/footer.php');
        return $t_1.$this->addContent($nameTemplate,$arraydata).$t_2;
    }

    private function addContent($nameTemplate, $arraydata){
        return require (__DIR__.'/view/'.$nameTemplate.'.php');
    }
}