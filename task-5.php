<?php

$a = ['asd', 'qwe', 'sda', 'wqe', 'dsa', 'qwer', 'afds', 'zxc'];
$a_new = [];

$i = 0;
while ($a) {
    $word_1 = array_shift($a);
    $a_new[$i][] = $word_1;
    foreach ($a as $k => $word_2) {
        if (strlen($word_1) == strlen($word_2)) {
            if (CheckWordWord($word_1, $word_2)) {
                if (!in_array($word_2, $a_new[$i])) {
                    $a_new[$i][] = $word_2;
                    unset($a[$k]);
                }
            }
        }
    }
    $i++;
}

/**
 * @param $word
 * @param $symbol
 * @return bool
 */
function CheckWordSymbol($word, $symbol)
{
    return mb_stristr($word, $symbol) ? true : false;
}

/**
 * @param $word_1
 * @param $word_2
 * @return bool
 */
function CheckWordWord($word_1, $word_2)
{
    $array_symbol = str_split($word_1);
    foreach ($array_symbol as $e) {
        if (!CheckWordSymbol($word_2, $e)) {
            return false;
        }
    }
    return true;
}



echo "<pre>";
print_r($a_new);
echo "<pre>";
echo "<hr/>";

echo '<br/><a href="/"><h3>Вернуться к заданию</h3></a><br/>';
