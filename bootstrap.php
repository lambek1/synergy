<?php


class bootstrap
{
    private $dsn;
    private $dbname;
    private $host;
    private $user;
    private $password;
    private $db;

    /**
     * bootstrap constructor.
     */
    public function __construct()
    {
        $path_configuration = __DIR__ . "/config.ini";
        if (file_exists($path_configuration)) {
            $array_config = parse_ini_file($path_configuration);
        } else {
            echo "Убедитесь? что файл конфига существует и лежит в корне проекта. <br/>";
        }

        $this->dsn = $array_config['dsn'];
        $this->dbname = $array_config['dbname'];
        $this->host = $array_config['host'];
        $this->user = $array_config['user'];
        $this->password = $array_config['password'];

        try {
            $this->db = new PDO($this->dsn . ':dbname=' . $this->dbname . ';host=' . $this->host, $this->user, $this->password);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    /**
     * @return PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return mixed
     */
    public function getDbname()
    {
        return $this->dbname;
    }


}