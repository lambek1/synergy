<?php

include_once 'bootstrap.php';
include_once 'Rend.php';

$bootstrap = new bootstrap();
$db = $bootstrap->getDb();
$view = new Rend();
$selected = (isset($_GET ['select']) && is_numeric($_GET ['select'])) ? " LIMIT 0," . $_GET ['select'] : ' LIMIT 0,10;';

$str = 'SELECT S.*,L.*  FROM `students` as S
        LEFT JOIN `likes` as L on L.`like_ID` = S.`ID` or L.`liked_ID` = S.`ID`
        WHERE L.`like_ID` is null AND L.`liked_ID` is null'.$selected;

$table = $db->query($str, PDO::FETCH_ASSOC)->fetchAll();

echo (string)$view->view("task", [
    'selected' =>isset($_GET ['select']) ? $_GET ['select'] : 10,
    'table' => $table,
    'task' => "Вернуть имена и средний балл всех студентов, которые не лайкали чужие страницы и не были лайкнуты другими пользователями.",
    'str' => $str,
]);
