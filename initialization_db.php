<?php

include_once "bootstrap.php";

class initialization_db
{
    private $db;
    public $message;

    /**
     * initialization_db constructor.
     */
    public function __construct()
    {
        $bootstrap = new bootstrap();
        $this->db = $bootstrap->getDb();
        if (!$this->db->query('SHOW TABLES;', PDO::FETCH_ASSOC)->fetchAll()) {
            if ($this->CreateTable($bootstrap->getDbname())) {
                if ($this->FillTableStudents()) {
                    $this->FillTableLikes();
                }
                $this->FillTableNews();
            }
        }
    }

    /**
     * @param $dbname
     * @return bool
     */
    protected function CreateTable($dbname)
    {
        $array_tbl = [
            "students" => "CREATE TABLE `" . $dbname . "`.`students` ( `ID` INT(11) NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL COMMENT 'имя' , `grade` TINYINT(2) NOT NULL COMMENT 'средний балл' , PRIMARY KEY (`ID`)) ENGINE = InnoDB;",
            "likes" => "CREATE TABLE `" . $dbname . "`.`likes` ( `like_ID` INT(11) NOT NULL COMMENT 'это ID того, кто поставил лак' , `liked_ID` INT(11) NOT NULL COMMENT 'ID того, кого лайкнули' ) ENGINE = InnoDB;",
            "news" => "CREATE TABLE `" . $dbname . "`.`news` ( `ID` INT(11) NOT NULL AUTO_INCREMENT , `name_news` VARCHAR(255) NOT NULL COMMENT 'Заголовок новости' , `text_news` TEXT NOT NULL COMMENT 'Текст новости' , `date_publication` DATETIME NOT NULL COMMENT 'Дата публикации' , PRIMARY KEY (`ID`)) ENGINE = InnoDB;"
        ];

        foreach ($array_tbl as $k => $e) {
            if ($this->db->query($e)) {
                $this->message .= "Таблица " . $k . " в базе данных " . $dbname . " создана успешно <br/>";
            } else {
                $this->message .= "Ошибка создана в таблица " . $k . " базы данных " . $dbname . "<br/>";
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function FillTableStudents()
    {
        $pach_file = __DIR__ . "/name.txt";
        if (!file_exists($pach_file)) {
            $this->message .= "Файл с именами для заполнения таблица не найден, проверьте  name.txt  в корне проекта. <br/>";
            return false;
        }
        $array = file($pach_file);
        $array_data = array_map(function ($e) {
            return [$e, (rand(1, 10))];
        }, $array);

        $this->FillDB("students", ["name", "grade"], $array_data);
        return true;
    }

    /**
     * @return bool
     */
    protected function FillTableLikes()
    {
        if (!$data = $this->db->query('SELECT ID FROM students limit 5,5000', PDO::FETCH_ASSOC)->fetchAll()) {
            $this->message .= "Ошибка таблица students не заполнена. <br/>";
            return false;
        }
        $this->count_students = count($data);
        $array_data_r = [];
        $i = 3;
        do {
            $array_data = array_map(function ($e) {
                $_like_id = rand(10, $this->count_students);
                if ($_like_id == $e['ID']) {
                    $_like_id = rand(1, $this->count_students);
                }
                return [$e['ID'], $_like_id];
            }, $data);
            $array_data_r = array_merge($array_data_r, $array_data);
        } while (--$i);
        $this->FillDB("likes", ["like_ID", "liked_ID"], $array_data_r);
        return true;
    }

    /**
     * @param $name_table
     * @param $array_name
     * @param $array_data
     * @return bool
     */
    private function FillDB($name_table, $array_name, $array_data)
    {
        $query = $this->db->prepare("INSERT INTO " . $name_table . "(" . implode(",", $array_name) . ") VALUES(?,?)");
        foreach ($array_data as $e) {
            if (!$query->execute($e)) {
                $this->message .= "При заполнении таблицы " . $name_table . " произошла ошибка. <br/>";
                return false;
            }
        }
        $this->message .= "Таблица " . $name_table . " заполнена успешно. <br/> ";
        return true;
    }


   protected function FillTableNews(){
       $a = $this->db->query("INSERT INTO `news`(`name_news`, `text_news`, `date_publication`) VALUES
            ('N-1','aaaaaaaaaaaa','2019-10-01'),
            ('N-2','aaaaaaaaaaaa','2019-10-02'),
            ('N-3','aaaaaaaaaaaa','2019-10-03'),
            ('N-4','aaaaaaaaaaaa','2019-10-04'),
            ('N-5','aaaaaaaaaaaa','2019-10-05'),
            ('N-6','aaaaaaaaaaaa','2019-10-06'),
            ('N-7','aaaaaaaaaaaa','2019-10-07'),
            ('N-8','aaaaaaaaaaaa','2019-10-08'),
            ('N-9','aaaaaaaaaaaa','2019-10-09'),
            ('N-10','aaaaaaaaaaaa','2019-10-10'),
            ('N-11','aaaaaaaaaaaa','2019-10-11'),
            ('N-12','aaaaaaaaaaaa','2019-10-12'),
            ('N-13','aaaaaaaaaaaa','2019-10-13'),
            ('N-14','aaaaaaaaaaaa','2019-10-14'),
            ('N-15','aaaaaaaaaaaa','2019-10-15'),
            ('N-16','aaaaaaaaaaaa','2019-10-16'),
            ('N-17','aaaaaaaaaaaa','2019-10-17'),
            ('N-18','aaaaaaaaaaaa','2019-10-18'),
            ('N-19','aaaaaaaaaaaa','2019-10-19'),
            ('N-20','aaaaaaaaaaaa','2019-10-20'),
            ('N-21','aaaaaaaaaaaa','2019-10-21'),
            ('N-22','aaaaaaaaaaaa','2019-10-22'),
            ('N-23','aaaaaaaaaaaa','2019-10-23'),
            ('N-24','aaaaaaaaaaaa','2019-10-24'),
            ('N-25','aaaaaaaaaaaa','2019-10-24'),
            ('N-26','aaaaaaaaaaaa','2019-10-24'),
            ('N-26','aaaaaaaaaaaa','2019-10-25'),
            ('N-28','aaaaaaaaaaaa','2019-10-25'),
            ('N-29','aaaaaaaaaaaa','2019-10-25'),
            ('N-30','aaaaaaaaaaaa','2019-10-26');");
       if($a){
           $this->message .= "Таблица News заполнена успешно. <br/> ";
       }else{
           $this->message .= " Ошибка таблица News не заполнена. <br/> ";
       }
       return;
    }

}