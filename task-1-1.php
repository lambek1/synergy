<?php

include_once 'bootstrap.php';
include_once 'Rend.php';

$bootstrap = new bootstrap();
$db = $bootstrap->getDb();
$view = new Rend();

$selected = (isset($_GET ['select']) && is_numeric($_GET ['select']))? " LIMIT 0,".$_GET ['select']: ' LIMIT 0,10;';

$str = 'SELECT S.ID,S.name, S.grade FROM `students` as S
INNER JOIN 
(SELECT `liked_ID` as `ID` FROM (SELECT `liked_ID`, count(`liked_ID`) as `c` FROM `likes` GROUP BY `liked_ID`) as T_1 WHERE T_1.`c`>1) as L
on S.ID = L.ID'.$selected;

$table = $db->query($str, PDO::FETCH_ASSOC)->fetchAll();
echo (string)$view->view("task", [
    'selected' => isset($_GET ['select']) ? $_GET ['select']: 10,
    'table' => $table,
    'task' => "Получить имена и средний балл всех студентов, которые были \"лайкнуты\" более чем одним студентом.",
    'str' => $str,
]);
